var fs = require('fs')
var express = require('express')

var app = express();

app.use(express.json());

app.get('/students', function(req,res){
    fs.readFile('./data.txt', function(err,data){
        if(err){
            res.send(err)
            return
        }
        res.send(JSON.parse(data.toString()))
    })
})

app.get('/student/:name', function(req,res){
    fs.readFile('./data.txt', function(err,data){
        if(err){
            res.send(err)
            return
        }
        var dataObjList = JSON.parse(data.toString())
        var filteredObjList = dataObjList.filter( (item) => item.name.toLowerCase()===req.params.name.toLowerCase() )
        if(filteredObjList.length!=0)
            res.json(filteredObjList[0])
        else
            res.send("Student not found!")
    })
})

app.get('/student', function(req,res){
    fs.readFile('./data.txt', function(err,data){
        if(err){
            res.send(err)
            return
        }
        var dataObjList = JSON.parse(data.toString())
        var filteredObjList
        if(req.query.age)
            filteredObjList = dataObjList.filter( (item) => item.age===req.query.age )
        else if(req.query.gender)
            filteredObjList = dataObjList.filter( (item) => item.gender.toLowerCase()===req.query.gender.toLowerCase() )

        if(filteredObjList.length==1)
            res.send(filteredObjList[0])
        else if(filteredObjList.length>1)
            res.send(filteredObjList)
        else
            res.send("Student not found!")
    })
})

app.post('/student', function(req,res){
    fs.readFile('./data.txt', function(err, data){
        if(err){
            res.send(err)
            return
        }
        var dataObjList = JSON.parse(data.toString())  
        dataObjList.push(req.body)
        fs.writeFile('./data.txt',JSON.stringify(dataObjList), function(){
            res.send("Student added.")
        })
    })
})

app.put('/student/:name', function(req,res){
    fs.readFile('./data.txt', function(err, data){
        var found = false;
        if(err){
            res.send(err)
            return
        }
        var dataObjList = JSON.parse(data.toString())  
        dataObjList.forEach(element => {
            if(element.name.toLowerCase()==req.params.name.toLowerCase()){
                found=true;
                element.age=req.body.age;
            }
        });
        if(found){
            fs.writeFile('./data.txt',JSON.stringify(dataObjList), function(){
                res.send("Student details updated.")
            })
        }
        else{
            res.send("Student not found!")
        }
    })
})

app.delete('/student/:name', function(req,res){
    fs.readFile('./data.txt', function(err,data){
        if(err){
            res.send(err)
            return
        }
        var index;
        var dataObjList = JSON.parse(data.toString())
        dataObjList.forEach(element => {
            if(element.name.toLowerCase()==req.params.name.toLowerCase()){
                index = dataObjList.indexOf(element);
            }
        });
        if(index){
            dataObjList.splice(index,1)
            fs.writeFile('./data.txt',JSON.stringify(dataObjList), function(){
                res.send("Student deleted.")
            })
        }
        else{
            res.send("Student not found!")
        }
    })
})

app.listen(9000, () => {console.log("Server started")})